//
//  AddSkillCell.swift
//  SurfProjectTest
//
//  Created by Wi-Fai on 01.08.2023.
//

import UIKit

class AddSkillCell: UICollectionViewCell {
    
    static let identifier = "AddSkill"
    
    private let plusImage = UIImageView()
         
     override init(frame: CGRect) {
         super.init(frame: frame)
         contentView.backgroundColor = .gray
         contentView.layer.cornerRadius = 22

         contentView.addSubview(plusImage)
         plusImage.translatesAutoresizingMaskIntoConstraints = false
         
         custom()
         constraint()
     }
     
     required init?(coder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
     }
         
   private  func custom() {
       plusImage.image = UIImage(named: "Plus")
     }
     
    private func constraint(){
         NSLayoutConstraint.activate([
            contentView.heightAnchor.constraint(equalToConstant: 70),
            plusImage.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            plusImage.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
            plusImage.heightAnchor.constraint(equalToConstant: 30),
            plusImage.widthAnchor.constraint(equalToConstant: 30),
        ])
     }
}

