//
//  WorkListCell.swift
//  SurfProjectTest
//
//  Created by Wi-Fai on 01.08.2023.
//

import UIKit

final class FirstProfileCell: UICollectionViewCell {
    
    static let identifier = "Profile"
    
    private let head = UILabel()
    private let locaImage = UIImageView()
    
    var locationBlock = UIView()

    let photo = UIImageView()
    var name = UILabel()
    var jobTitle = UILabel()
    var location = UILabel()
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.backgroundColor = UIColor(red: 243, green: 243, blue: 245, alpha: 1)
        addOnView()
        customElement()
        constraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private  func customElement() {
        head.text = "Профиль"
        head.textColor = .black
        head.textAlignment = .center
        head.font = .boldSystemFont(ofSize: 16)
        
        photo.layer.masksToBounds = true
        photo.layer.cornerRadius = 60
        
        name.textAlignment = .center
        name.textColor = .black
        name.font = .boldSystemFont(ofSize: 24)
        name.backgroundColor = .white
        name.numberOfLines = 2
        
        jobTitle.font = .systemFont(ofSize: 14)
        jobTitle.textColor = .gray
        jobTitle.textAlignment = .center
        jobTitle.backgroundColor = .white
        jobTitle.numberOfLines = 2
        
        locaImage.image = UIImage(named: "location")
                
        location.font = .systemFont(ofSize: 14)
        location.textColor = .gray
        location.textAlignment = .left
        location.backgroundColor = .white
        location.numberOfLines = 1
    }
    
    private func constraint(){
        NSLayoutConstraint.activate([
            head.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 18),
            head.widthAnchor.constraint(equalToConstant: 72),
            head.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            head.heightAnchor.constraint(equalToConstant: 20),
            
            photo.topAnchor.constraint(equalTo: head.bottomAnchor, constant: 30),
            photo.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            photo.heightAnchor.constraint(equalToConstant: 120),
            photo.widthAnchor.constraint(equalToConstant: 120),
            
            name.topAnchor.constraint(equalTo: photo.bottomAnchor, constant: 20),
            name.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            name.heightAnchor.constraint(equalToConstant: 80),
            name.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: contentView.frame.width/6),
            name.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -contentView.frame.width/6),
            
            jobTitle.topAnchor.constraint(equalTo: name.bottomAnchor, constant: 2),
            jobTitle.heightAnchor.constraint(equalToConstant: 40),
            jobTitle.leadingAnchor.constraint(equalTo: name.leadingAnchor),
            jobTitle.trailingAnchor.constraint(equalTo: name.trailingAnchor),
            
            locationBlock.topAnchor.constraint(equalTo: jobTitle.bottomAnchor, constant: 5),
            locationBlock.heightAnchor.constraint(equalToConstant: 16),
            locationBlock.widthAnchor.constraint(equalToConstant: 110),
            locationBlock.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
            
            locaImage.leadingAnchor.constraint(equalTo: locationBlock.leadingAnchor, constant: 0),
            locaImage.topAnchor.constraint(equalTo: locationBlock.topAnchor, constant: 0),
            locaImage.bottomAnchor.constraint(equalTo: locationBlock.bottomAnchor, constant: 0),
            locaImage.widthAnchor.constraint(equalToConstant: 16),
            locaImage.heightAnchor.constraint(equalToConstant: 16),
            
            location.leadingAnchor.constraint(equalTo: locaImage.trailingAnchor, constant: 3),
            location.heightAnchor.constraint(equalToConstant: 16),
            location.topAnchor.constraint(equalTo: locationBlock.topAnchor, constant: 0)
        ])
    }
    
    private func addOnView() {
        contentView.addSubview(head)
        head.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(photo)
        photo.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(name)
        name.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(jobTitle)
        jobTitle.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(locationBlock)
        locationBlock.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(locaImage)
        locaImage.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(location)
        location.translatesAutoresizingMaskIntoConstraints = false
    }
}
