//
//  ViewController.swift
//  SurfProjectTest
//
//  Created by Wi-Fai on 01.08.2023.
//

import UIKit

class MainCollectionView: UIViewController {
    
    var collectionView: UICollectionView!
    
    let data = PersonData()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        createCollectionView()
    }
}

extension MainCollectionView: UICollectionViewDataSource,
                              UICollectionViewDelegate,
                              UICollectionViewDelegateFlowLayout {
    
    private func createCollectionView() {
        collectionView = UICollectionView(frame: .zero,
                                          collectionViewLayout: createCompositionalLayout())

        collectionView.backgroundColor = .white
        
        collectionView.dataSource = self
        collectionView.delegate = self

        view.addSubview(collectionView)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        
        collectionView.register(FirstProfileCell.self,
                                forCellWithReuseIdentifier: "\(FirstProfileCell.self)")
        collectionView.register(SecondSkillCell.self,
                                forCellWithReuseIdentifier: "\(SecondSkillCell.self)")
        collectionView.register(ThirdAboutSelfCell.self,
                                forCellWithReuseIdentifier: "\(ThirdAboutSelfCell.self)")
        collectionView.register(EditButtonCell.self,
                                forCellWithReuseIdentifier: "\(EditButtonCell.self)")
        collectionView.register(AddSkillCell.self,
                                forCellWithReuseIdentifier: "\(AddSkillCell.self)")


        
        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0)
        ])
    }
    
    private func createCompositionalLayout() -> UICollectionViewCompositionalLayout {
        //item
        let item = CompositionalLayout.createItem(width: .fractionalWidth(1),
                                                  height: .estimated(380),
                                                  spacing: 10)

        //group
        let group = NSCollectionLayoutGroup.horizontal(layoutSize:
                                                        NSCollectionLayoutSize(widthDimension:.estimated(view.frame.width),
                                                                               heightDimension: .estimated(100)),
                                                       subitem: item,
                                                       count:1)

        //section
        
        let section = NSCollectionLayoutSection(group: group)
        section.contentInsets = NSDirectionalEdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0)

        return UICollectionViewCompositionalLayout(section: section)
    }
    
    private func createFlowLayout()  -> UICollectionViewFlowLayout {
        let layout = UICollectionViewFlowLayout()
         layout.scrollDirection = .vertical
         layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
         layout.minimumLineSpacing = 10
         layout.minimumInteritemSpacing = 0
         layout.sectionInset = .init(top: 10, left: 10, bottom: 30, right: 10)
        return layout
    }
    
  //MARK: CollectionView delegate and DataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch section {
        case 0: return 1
        case 1: return data.skillArray.count + 1
        case 2: return 1
        default:
            return 1
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        3
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == data.skillArray.count {
            print("alert")
        
            let alert = UIAlertController(title: "Добавление навыка", message: "Какими навыками ты еще владеешь", preferredStyle: .alert)
            let actionYes = UIAlertAction(title: "Add", style: .default) {_ in
                
                collectionView.reloadData()
            }
            
            alert.addTextField()
            let actionNo = UIAlertAction(title: "No", style: .destructive)
            alert.addAction(actionYes)
            alert.addAction(actionNo)
            present(alert, animated: true)

        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        switch indexPath.section {
        case 0:
            let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(FirstProfileCell.self)",
                                                              for: indexPath) as? FirstProfileCell
            itemCell?.name.text = data.name
            itemCell?.photo.image = UIImage(named: data.photo)
            itemCell?.jobTitle.text = data.jobTitle
            itemCell?.location.text = data.location
            return itemCell!
            
        case 1:
            let stepsCount = data.skillArray.count
            
            let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(SecondSkillCell.self)",
                                                              for: indexPath) as? SecondSkillCell
            
            if indexPath.row == 0 {
                let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(EditButtonCell.self)",
                                                                  for: indexPath) as? EditButtonCell
                return itemCell!
            }

            if data.skillArray.isEmpty{
                let addCell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(AddSkillCell.self)",
                                                                 for: indexPath) as? AddSkillCell
                return addCell!
            } else if data.skillArray.count != 0 {
                
                switch indexPath.row {
                case stepsCount:
                    let addCell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(AddSkillCell.self)",
                                                                      for: indexPath) as? AddSkillCell
                    return addCell!
                default:
                    itemCell?.skillName.text = data.skillArray[indexPath.row]
                    return itemCell!
                }
            } else {
                return itemCell!
            }
            
        case 2:
            let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(ThirdAboutSelfCell.self)",
                                                              for: indexPath) as? ThirdAboutSelfCell
            itemCell?.textField.text = data.aboutSelfText
            return itemCell!
        default:
            let itemCell = collectionView.dequeueReusableCell(withReuseIdentifier: "\(FirstProfileCell.self)",
                                                              for: indexPath) as? FirstProfileCell
            return itemCell!
        }
    }
}


//MARK: WorkLisrCollection

struct CompositionalLayout {
    static func createItem (width: NSCollectionLayoutDimension,
                            height: NSCollectionLayoutDimension,
                            spacing: CGFloat
    ) -> NSCollectionLayoutItem {

        let item = NSCollectionLayoutItem(layoutSize: NSCollectionLayoutSize(widthDimension: width,
                                                                             heightDimension: height))
        item.contentInsets = NSDirectionalEdgeInsets(top: spacing, leading: spacing, bottom: spacing, trailing: spacing)

        return item
    }
}
