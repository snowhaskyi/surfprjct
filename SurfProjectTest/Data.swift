//
//  Data.swift
//  SurfProjectTest
//
//  Created by Wi-Fai on 01.08.2023.
//

struct PersonData {
    let name: String = "Годунов Владимир Игоревич"
    let photo = "Photo"
    let jobTitle = "Cтажер iOS-разработчик, опыт от 1-го года"
    let location: String = "Калининград"
    
    let aboutSelfText = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
    
    var skillArray = ["MVI/MVVM", "CoreData", "SOLID", "Custom View", "VIPER"]
    //var skillArray: [String] = []
}


enum EditButtonState {
    case on
    case off
}

var buttonState: EditButtonState = .on
