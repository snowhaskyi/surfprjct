//
//  EditButtonCell.swift
//  SurfProjectTest
//
//  Created by Wi-Fai on 01.08.2023.
//

import UIKit

class EditButtonCell: UICollectionViewCell {
    
    static let identifier = "EditButton"
    
    private let addButton = UIButton()
    private var name = UILabel()
         
     override init(frame: CGRect) {
         super.init(frame: frame)
         contentView.backgroundColor = .white
         
         contentView.addSubview(name)
         contentView.addSubview(addButton)
         name.translatesAutoresizingMaskIntoConstraints = false
         addButton.translatesAutoresizingMaskIntoConstraints = false
         
         custom()
         constraint()
     }
     
     required init?(coder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
     }
         
   private  func custom() {
       name.text = "Мои навыки"
       name.backgroundColor = .white
       name.font = .boldSystemFont(ofSize: 16)
       
       addButton.titleLabel?.text = "add"
       addButton.setImage(UIImage(named: "edit"), for: .normal)
       addButton.addTarget(self, action:#selector(addSkill), for: .touchUpInside)

     }
     
    private func constraint(){
         NSLayoutConstraint.activate([
             contentView.heightAnchor.constraint(equalToConstant: 50),
             name.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
             name.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
             name.widthAnchor.constraint(equalToConstant: 150),
             name.heightAnchor.constraint(equalToConstant: 20),
             
             addButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 0),
             addButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
             addButton.heightAnchor.constraint(equalToConstant: 30),
             addButton.widthAnchor.constraint(equalToConstant: 30),
        ])
     }
    
    @objc func addSkill() {
        print("Skill")
        //change state and reload section
    }
}
