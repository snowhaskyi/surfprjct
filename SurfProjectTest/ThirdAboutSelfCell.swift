//
//  ThirdAboutSelfCell.swift
//  SurfProjectTest
//
//  Created by Wi-Fai on 01.08.2023.
//

import UIKit

class ThirdAboutSelfCell: UICollectionViewCell {
    
    static let identifier = "AboutSelf"

   private let title = UILabel()
    let textField = UITextView()
     
     override init(frame: CGRect) {
         super.init(frame: frame)
         contentView.backgroundColor = UIColor(red: 243, green: 243, blue: 245, alpha: 1)
         
         contentView.addSubview(title)
         title.translatesAutoresizingMaskIntoConstraints = false
         contentView.addSubview(textField)
         textField.translatesAutoresizingMaskIntoConstraints = false

         custom()
         constraint()
     }
     
     required init?(coder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
     }
         
   private  func custom() {
       title.text = "О себе"
       title.textAlignment = .left
       title.backgroundColor = .white
       title.textColor = .black
       title.font = .systemFont(ofSize: 16)
       
       textField.font = .systemFont(ofSize: 14)
       textField.backgroundColor = UIColor(red: 243, green: 243, blue: 245, alpha: 1)
     }
     
    private func constraint(){
         NSLayoutConstraint.activate([
             contentView.heightAnchor.constraint(equalToConstant: 70),
             title.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
             title.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
             title.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
             
             textField.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 15),
             textField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 20),
             textField.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -20),
             textField.heightAnchor.constraint(equalToConstant: 800)
         ])
     }

}
