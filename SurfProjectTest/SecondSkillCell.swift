//
//  SecondSkillCell.swift
//  SurfProjectTest
//
//  Created by Wi-Fai on 01.08.2023.
//

import UIKit

class SecondSkillCell: UICollectionViewCell {
    
    static let identifier = "Skill"

     var skillName = UILabel()
    private let addSkill = UIButton()
     
     override init(frame: CGRect) {
         super.init(frame: frame)
         contentView.backgroundColor = .gray
         contentView.layer.cornerRadius = 22

         
         contentView.addSubview(skillName)
         contentView.addSubview(addSkill)
         skillName.translatesAutoresizingMaskIntoConstraints = false
         addSkill.translatesAutoresizingMaskIntoConstraints = false
         
         custom()
         constraint()
     }
     
     required init?(coder: NSCoder) {
         fatalError("init(coder:) has not been implemented")
     }
         
   private  func custom() {       
       if buttonState == .off {
           addSkill.isHidden = true
       } else {
           addSkill.setImage(UIImage(named: "x"), for: .normal)
       }
     }
     
    private func constraint(){
         NSLayoutConstraint.activate([
             contentView.heightAnchor.constraint(equalToConstant: 70),
             skillName.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
             skillName.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -10),
             skillName.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
             
             addSkill.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
             addSkill.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
             addSkill.heightAnchor.constraint(equalToConstant: 15),
             addSkill.widthAnchor.constraint(equalToConstant: 15),
        ])
     }

}
